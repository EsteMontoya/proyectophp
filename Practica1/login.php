<?php
	session_start();

?>


<!DOCTYPE html>
<html lang = "es">
<head>
	<title>Inicio de sesión</title>
	<!--Bootsrap 4-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Iconos-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="icon" type="image/png" href="./img/icono.png" />

	<!--CSS-->
	<link rel="stylesheet" type="text/css" href="./css/style.css">

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif:wght@700&display=swap" rel="stylesheet"> 


</head>
<body id = "login">
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Inicio sesión</h3>
			</div>
			<div class="card-body">
				<form name = "inicioSesion" method = "post" action = "loginController.php">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input name = "usuario" type="text" class="form-control" placeholder="Usuario" required autofocus>
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input name = "contrasena" type="password" class="form-control" placeholder="Contraseña" required>
					</div>
					<div class="form-group">
						<input type="submit" value="Ingresar" class="btn float-right login_btn">
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>
</body>
</html>

<?php
	if(isset($_GET["fallo"]) && $_GET["fallo"] == 'true')
	{
		echo'<script type="text/javascript">
		window.alert("Contraseña o usuario invalidos")
		</script>';
	}
?>
