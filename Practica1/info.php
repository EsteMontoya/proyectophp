<?php
  session_start();
  if(isset($_GET["id"])) {
    for ($iCont = 1 ; $iCont <= sizeof($_SESSION['Alumno']) && $iCont != $_GET["id"] ; $iCont++){
      next($_SESSION['Alumno']);
    }
    $usuario = current($_SESSION['Alumno']);
	} else {
    header('Location: '."./login.php");
  }
  
?>
<!DOCTYPE html>
<html lang = "es">
<head>
	<title>Pagina Principal</title>
	<!--Bootsrap 4-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Iconos-->
    <link rel="icon" type="image/png" href="./img/icono.png" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--CSS-->
    <link rel="stylesheet" type="text/css" href="./css/style.css">

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif:wght@700&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet"> 

</head>
<body id = "informacion">
<div class="container">

<!-- Barra de navegación -->

<nav class="navbar navbar-expand-lg navbar-light sticky-top navbar-color ">
  <a class="navbar-brand" href="./info.php"><i class="fas fa-home navbar-item"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link navbar-item" href="./formulario.php">Registrar alumnos<span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link navbar-item" href="./login.php">Cerrar sesión<span class="sr-only"></span></a>
      </li>
    </ul>
  </div>
</nav>

<div class="jumbotron">
  <h3>Datos del usuario</h3>
  <hr class="">
  <br>

  <?php
    echo ("<h4>Usuario: ".$usuario['nombre']."</h4><br>");
    echo ("<h4>Fecha de nacimiento: ".$usuario['fechaNac']."</h4><br>");
    echo ("<h4>Número de cuenta: ".$usuario['numCuenta']."</h4><br>");
  ?>

</div>

<div class="jumbotron">
<h3>Datos registrados</h3>
  <hr class="">
<table class="table table-bordered">
  <thead class = "thead-color">
    <tr>
      <th class = "columna" scope="col">#</th>
      <th class = "columna" scope="col">Número de cuenta</th>
      <th class = "columna" scope="col">Nombre</th>
      <th class = "columna" scope="col">Fecha de nacimiento</th>
    </tr>
  </thead>
  <tbody >

    <?php
      for($iCont = 1; $iCont <= sizeof($_SESSION['Alumno']) ; $iCont++){
        echo('
        <tr>
        <th scope="row">'.$_SESSION["Alumno"][$iCont]["numCuenta"].'</th>
        <td>'.$_SESSION["Alumno"][$iCont]["numCuenta"].'</td>
        <td>'.$_SESSION["Alumno"][$iCont]["nombre"].' '.$_SESSION["Alumno"][$iCont]["primerApellido"].' '.$_SESSION["Alumno"][$iCont]["segundoApellido"].'</td>
        <td>'.$_SESSION["Alumno"][$iCont]["fechaNac"].'</td>
        </tr>');
      }
    ?>
  </tbody>
</table>
</div>

</div>
</body>
</html> 