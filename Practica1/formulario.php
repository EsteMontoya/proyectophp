<!DOCTYPE html>
<html lang = "es">
<head>
	<title>Pagina Principal</title>
	<!--Bootsrap 4-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Iconos-->
    <link rel="icon" type="image/png" href="./img/icono.png" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--CSS-->
    <link rel="stylesheet" type="text/css" href="./css/style.css">

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif:wght@700&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet"> 

</head>
<body id = "informacion">
<div class="container">

<!-- Barra de navegación -->

<nav class="navbar navbar-expand-lg navbar-light sticky-top navbar-color ">
  <a class="navbar-brand" href="./info.php"><i class="fas fa-home navbar-item"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link navbar-item" href="#">Registrar alumnos<span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link navbar-item" href="#">Cerrar sesión<span class="sr-only"></span></a>
      </li>
    </ul>
  </div>
</nav>

<div class = "elemento">
<form name = "registrarAlumno"  method = "post" action = "formularioController.php">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="num_cuenta">Número de cuenta</label>
      <input id="num_cuenta" name = "num_cuenta" type="number" class="form-control"  placeholder="123456789" required>
    </div>

    <div class="form-group col-md-4">
      <label for="nombre">Nombre(s)</label>
      <input id ="nombre" name="nombre" type="text" class="form-control" placeholder="Juan" required>
    </div>
    
    <div class="form-group col-md-4">
      <label for="primer_apellido">Primer apellido</label>
      <input id ="primer_apellido" name="primer_apellido" type="text" class="form-control" placeholder="Martinez" required>
    </div>

    <div class="form-group col-md-6">
      <label for="segundo_apellido">Segundo apellido</label>
      <input id ="segundo_apellido" name="segundo_apellido" type="text" class="form-control" placeholder="Martinez">
    </div>

    <div class="form-group col-md-6">
      <label for="fec_nac">Fecha de nacimiento</label>
      <input id ="fec_nac" name="fec_nac" type="date" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
      <label for="contrasena">Contraseña</label>
      <input name ="contrasena" id="contrasena" type="password" class="form-control" placeholder="Password" required>
    </div>

    <div class="form-group col-md-6">
      <label for="Genero">Género</label>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="genero" id="genero1" value="M" checked>
        <label class="form-check-label" for="genero1">Mujer</label>
      </div>

      <div class="form-check">
        <input class="form-check-input" type="radio" name="genero" id="genero2" value="H">
        <label class="form-check-label" for="genero2">Hombre</label>
      </div>

      <div class="form-check">
        <input class="form-check-input" type="radio" name="genero" id="genero3" value="O">
        <label class="form-check-label" for="genero3">Otro</label>
      </div>
    </div>

      
  </div>
  <button type="submit" class="btn btn-primary">Registrar</button>
</form>
</div>

</div>
</body>
</html> 