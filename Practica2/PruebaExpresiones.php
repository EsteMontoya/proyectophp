<!DOCTYPE html>
<html lang="es">
<head>
    <title>Practica 2: Expresiones regulares</title>
    <h1>Expresiones regulares</h1>
</head>
<body>
    <ol>
        <li> Realizar una expresión regular que detecte emails correctos.</li>
            <dd>
                <?php
                    $correoCorrecto = "/^(^[a-z A-Z 0-9 !-\/ :-?]*)*@(([a-z A-Z 0-9]*\.))*\D{1,4}$/";
                    echo("El correo electronico esteban.montoya@alumno.cch.unam.mx es: ".(preg_match($correoCorrecto, "esteban.montoya@alumno.cch.unam.mx") == 1 ? "Válido" : "Invalido"));
                ?>
            </dd>
        <br>
        <br>

        <li> Realizar una expresión regular que detecte emails correctos.</li>
            <dd>
                <?php
                    $curpCorrecto = "/^([a-z A-Z]{4,4})(\d{6,6})([a-z A-Z]{6,6})(\d{2,2})$/";
                    echo("El CURP MOME990905HMCNYS06 es: ".(preg_match($curpCorrecto, "MOME990905HMCNYS06") == 1 ? "Válido" : "Invalido"));
                ?>
            </dd>

        <br>
        <br>

        <li>Realizar una expresión regular que detecte palabras de longitud mayor a 50 formadas solo por letras.</li>
            <dd>
                <?php
                    $palabraCorrecta = "/^([a-z A-Z]{50,})$/";
                    echo("La palabra DJSCNDICNDCDCNJCCNDKJCNDKJDOCNDCsjxncndijcnkcsncjnc es: ".(preg_match($palabraCorrecta, "DJSCNDICNDCDCNJCCNDKJCNDKJDOCNDCsjxncndijcnkcsncjnc") == 1 ? "Válido" : "Invalido"));
                ?>
            </dd>

        <br>
        <br>

        <li>Crea una función para escapar los símbolos especiales.</li>
            <dd>
                <?php
                    $sinEspeciales = "/^([a-z A-Z 0-9 \s])*$/";
                    echo("La frase Sin especiales es: ".(preg_match($sinEspeciales, "Sin especiales") == 1 ? "Válido" : "Invalido"));
                ?>
            </dd>
        <br>
        <br>

        <li>Crea una función para escapar los símbolos especiales.</li>
            <dd>
                <?php
                    $esDecimal = "/^([0-9]{1,})\.([0-9]{1,})$/";
                    echo("El número 120.000012 es: ".(preg_match($esDecimal, "120.000012") == 1 ? "Decimal" : "No decimal"));
                ?>
            </dd>
        <br>
        <br>
    </ol>
</body>
</html>