<!DOCTYPE html> 
<html lang="es">
	<head> 
        <title>Rombo y piramide</title>       
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    </head> 
    
	<body id = "principal"> 
        <h1> Creando rombos y piramides</h1>
        <section class="main">
            <button id="btnPiramide" class = "btn" value="toggle">Crear piramide con PHP</button>
            <div id="piramide" class="figura">
                <?php 
                $maxFilas = 30;

                for ($iCont = 1 ; $iCont <= $maxFilas ; $iCont++){
                    for ($iPuntos = $iCont ; $iPuntos > 0 ; $iPuntos--){
                        print('<i class="fas fa-star-of-life"></i>');   
                    }
                    print("<br>");
                }
                ?>
            </div>
            <button id="btnRombo" class = "btn" value="toggle">Crear rombo con PHP</button>
            <div id="rombo" class="figura">
                <?php 
                    for ($iCont = 1 ; $iCont <= $maxFilas ; $iCont++){
                        for ($iPuntos = $iCont ; $iPuntos > 0 ; $iPuntos--){
                            print('<i class="fas fa-star-of-life"></i>');
                        }
                        print("<br>");
                    }
                    for (; $iCont >= 0 ; $iCont--){
                        for ($iPuntos = $iCont ; $iPuntos > 0 ; $iPuntos--){
                            print('<i class="fas fa-star-of-life"></i>');
                        }
                        print("<br>");
                    }
                ?>
            </div>
        </section>
    </body>
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="./js/Ejercicio 1.js"></script>
</html> 